const express = require('express');
const bodyparser = require('body-parser');

//dbConnection
require('./StudentApp/config/dbConnection');

//initialize our express app
const app = express();
/*
To handle HTTP POST request in Express.js version 4 and above, you need to install middleware module called body-parser.
body-parser extract the entire body portion of an incoming request stream and exposes it on req.body.
*/
app.use(bodyparser.urlencoded({ extended: false }));
app.use(bodyparser.json());

//routes
require('./StudentApp/config/routes')(app);

/*
app.get('/test', (req, res) =>{
    res.send('hello word');
});
*/

let port = 3000;
app.listen(port,() =>{
    console.log('Server is up and runing on port ' + port);
});