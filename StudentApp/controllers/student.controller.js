const Student = require('../models/student.model');
const Departement = require('../models/departement.model');

exports.findAll = (req, res) => {
    Student.find((err, students) => {
        if (err) return res.status(500).send(err.message || "Some error occurred while finding list of students.");
        res.send({students: students});
    })
};

exports.add = function (req, res) {
    /*Creation de l'objet student*/
    let student = new Student({
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        cin: req.body.cin,
        birthday: req.body.birthday,
        num_inscrip: req.body.num_inscrip,
        address: req.body.address,
        tel: req.body.tel,
        dep_id: req.body.dep_id
    });

    /*ajout du student*/
    student.save((saveErr, savedStudent) => {
        if (saveErr) return res.status(500).send(saveErr.message || "Some error occurred while creating the student.");
        res.status(201).send({data: savedStudent});
    });

    /*MAJ du _students du document departement*/
    Departement.findOne({code: req.body.dep_id}, function (err, data) {
        if (err) return res.status(500).send('Departement not found');
        data._students.push(student._id); // update ur values goes here
        var nvdep = new Departement(data);
        nvdep.save();
    });

};

exports.details = function (req, res) {
    Student.findById(req.params._id, function (err, data) {
        if (err) return res.status(500).send('Student not found');
        return res.status(200).send(data);
    });
};

exports.update = (req, res, next) => {
    Student.findById(req.params._id, (err, student) => {
        // This assumes all the fields of the object is present in the body.
        student.firstname = req.body.firstname;
        student.lastname = req.body.lastname;
        student.cin = req.body.cin;
        student.birthday = req.body.birthday;
        student.num_inscrip = req.body.num_inscrip;
        student.address = req.body.address;
        student.tel = req.body.tel;
        student.dep_id = req.body.dep_id;

        student.save((saveErr, updatedStudent) => {
            if (saveErr) return res.status(500).send(saveErr.message || "Some error occurred while updating the student.");
            res.send({data: updatedStudent});
        });

        /*MAJ du _students du document departement*/
    /*    Departement.findOne({code: req.body.dep_id}, function (err, data) {
            if (err) return res.status(500).send('Departement not found');
            data._students.push(student._id); // update ur values goes here
            var nvdep = new Departement(data);
            nvdep.save();
        });*/
    });
};

exports.delete = function (req, res) {
    Student.findById(req.params.id, (err, student) => {
        student.remove((studentErr, removedStudent) => {
            if (studentErr) return res.status(500).send(studentErr.message || "Some error occurred while deleting the student.");
            res.send({data: removedStudent});
        });
    });
};

