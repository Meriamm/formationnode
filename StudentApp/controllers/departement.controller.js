const Departement = require('../models/departement.model');

exports.findAll = (req, res) => {
    Departement.find().populate('_students').exec((err, departements) => {
        if (err) return res.status(500).send(err.message || "Some error occurred while finding list of departements.");
        res.send({departements: departements});
    })
};

exports.add = function (req, res) {
    let departement = new Departement({
        code: req.body.code,
        name: req.body.name
    });
    departement.save((saveErr, savedDepartement) => {
        if (saveErr) return res.status(500).send(saveErr.message || "Some error occurred while creating the departement.");
        res.status(201).send({data: savedDepartement});
    });
};

exports.details = function (req, res) {
    Departement.findById(req.params._id, function (err, data) {
        if (err) return res.status(500).send('Departement not found');
        return res.status(200).send(data);
    });
};

exports.update = (req, res, next) => {
    Departement.findById(req.params._id, (err, departement) => {
        // This assumes all the fields of the object is present in the body.
        departement.code = req.body.code;
        departement.name = req.body.name;

        departement.save((saveErr, updatedDepartement) => {
            if (saveErr) return res.status(500).send(saveErr.message || "Some error occurred while updating the departement.");
            res.send({data: updatedDepartement});
        });
    });
};

exports.delete = function (req, res) {
    Departement.findById(req.params.id, (err, departement) => {
        departement.remove((departementErr, removedDepartement) => {
            if (departementErr) return res.status(500).send(studentErr.message || "Some error occurred while deleting the departement.");
            res.send({data: removedDepartement});
        });
    });
};

