const mongoose = require('mongoose');

const url = 'mongodb://localhost:27017/StudentApp';

//connecting to the database
mongoose.connect(url, {useNewUrlParser: true, useUnifiedTopology:true}, null)
    .then(()=> {
        console.log("Successfully connected to MongoDB");
    }).catch(err => {
        console.log('could not connect to MongoDB');
        process.exit();
    },
);
mongoose.set('useFindAndModify', false);


// Configuring the database
mongoose.Promise = global.Promise;