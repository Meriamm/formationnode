var bodyParser = require('body-parser');
var jsonParser = bodyParser.json();

//routes definition
const student = require('../routes/student.route');
const departement = require('../routes/departement.route');
// routes
module.exports = function (app) {
    app.use('/api/student', jsonParser,  student);
    app.use('/api/departement', jsonParser,  departement);
};