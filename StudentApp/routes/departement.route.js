const express = require('express');
const router = express.Router();

//require the controllers which we did not create yet!
const Departement_controller = require('../controllers/departement.controller');
router.get('/', Departement_controller.findAll);
router.post('/add', Departement_controller.add);
router.get('/:_id', Departement_controller.details);
router.put('/:_id/update', Departement_controller.update);
router.delete('/:id/delete', Departement_controller.delete);

//url
module.exports = router;