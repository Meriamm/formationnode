const express = require('express');
const router = express.Router();

//require the controllers which we did not create yet!
const Student_controller = require('../controllers/student.controller');
router.get('/', Student_controller.findAll);
router.post('/add', Student_controller.add);
router.get('/:_id', Student_controller.details);
router.put('/:_id/update', Student_controller.update);
router.delete('/:id/delete', Student_controller.delete);

//url
module.exports = router;