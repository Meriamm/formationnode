const mongoose = require ('mongoose');
const Schema = mongoose.Schema;
const Departement = require('./departement.model');

let StudentSchema = new Schema({
    firstname: {type: String, required: true, max: 100},
    lastname: {type: String, required: true},
    cin: {type: Number, required: true},
    birthday: {type: String, required: true},
    num_inscrip: {type: Number, required: true},
    address: {type: String, required: true},
    tel: {type: Number, required: true}, 
    dep_id: {type: String, required: true}
});

//export the model 
module.exports = mongoose.model('Student', StudentSchema );