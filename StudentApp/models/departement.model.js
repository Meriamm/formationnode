const mongoose = require ('mongoose');
const Schema = mongoose.Schema;
const Student = require('./student.model');


let DepartementSchema = new Schema({
    name: {type: String, require: true},
    code: {type: String, require: true},
    _students: [{type: Schema.Types.ObjectId, ref:"Student", required: true}]
});

// export model
module.exports = mongoose.model('Departement', DepartementSchema);